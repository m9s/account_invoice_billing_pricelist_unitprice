# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.modules.account_invoice_billing.billing import _STATES, _DEPENDS
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    unit_price = fields.Numeric('Unit Price', digits=(16, 4), states=_STATES,
        depends=_DEPENDS)

    def __init__(self):
        super(BillingLine, self).__init__()
        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []
        for field in ['product', 'party', 'billing_date', 'company',
                'quantity']:
            if field not in self.product.on_change:
                self.product.on_change += [field]
        self.quantity = copy.copy(self.quantity)
        if self.quantity.on_change is None:
            self.quantity.on_change = []
        for field in ['product', 'party', 'billing_date', 'company',
                'quantity']:
            if field not in self.quantity.on_change:
                self.quantity.on_change += [field]
        self._reset_columns()
        self._rpc.update({
            'on_change_quantity': False
            })

    def on_change_product(self, vals):
        res = super(BillingLine, self).on_change_product(vals)
        res.update(self._get_price_from_vals(vals))
        return res

    def _get_price_from_vals(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        company_obj = pool.get('company.company')
        pricelist_obj = pool.get('pricelist.pricelist')

        res = {}
        if vals.get('product'):
            context = Transaction().context.copy()

            if vals.get('company'):
                company_id = vals['company']
            else:
                company_id = Transaction().context['company']
            company = company_obj.browse(company_id)
            context['currency'] = company.currency.id

            pricelist = self._get_pricelist_from_vals(vals)
            if pricelist:
                context['pricelist'] = pricelist.id

            if vals.get('billing_date'):
                context['price_date'] = vals['billing_date']
                context['date'] = vals['billing_date']

            with Transaction().set_context(**context):
                unit_price = pricelist_obj.get_price([vals['product']],
                    quantity=vals.get('quantity', 1))[vals['product']]

            res['unit_price'] = unit_price
        return res

    def _get_pricelist_from_vals(self, vals):
        party_obj = Pool().get('party.party')
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            return party.pricelist_sale
        return False

    def on_change_quantity(self, vals):
        res = self._get_price_from_vals(vals)
        return res

    def _get_unit_price_from_line(self, line):
        return line.unit_price

BillingLine()
