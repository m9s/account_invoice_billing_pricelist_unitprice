#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import Not, Equal, Eval

class InvoiceLine(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def __init__(self):
        super(InvoiceLine, self).__init__()
        self.unit_price = copy.copy(self.unit_price)
        if self.unit_price.states is None:
            self.unit_price.states = {
                    'invisible': Not(Equal(Eval('type'), 'line')),
                    }
        elif 'required' in self.unit_price.states:
            del self.unit_price.states['required']
        if 'type' not in self.unit_price.depends:
            self.unit_price.depends = copy.copy(self.unit_price.depends)
            self.unit_price.depends.append('type')
        self._reset_columns()

InvoiceLine()
