# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Pricelist Unitprice',
    'name_de_DE': 'Fakturierung Abrechnung Preislisten Einzelpreis',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds field unit price for billing with pricelists
    ''',
    'description_de_DE': '''
    - Fügt Feld Einzelpreis für die Abrechnung mit Preislisten hinzu.
    ''',
    'depends': [
        'account_invoice_billing_pricelist'
    ],
    'xml': [
        'billing.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
